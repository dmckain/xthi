# Overridable options
prefix=/usr
CC = gcc
MPICC= mpicc
CFLAGS = -std=c11 -Wall -Wpedantic -Wextra
LDFLAGS =
INSTALL = install

ALL_CFLAGS = -fopenmp $(CFLAGS)
ALL_LDFLAGS = -lnuma $(LDFLAGS)

all: xthi xthi.nompi

xthi: xthi.c
	$(MPICC) -o $@ $(ALL_CFLAGS) $< $(ALL_LDFLAGS)

xthi.nompi: xthi.c
	$(CC) -o $@ -DNO_MPI $(ALL_CFLAGS) $< $(ALL_LDFLAGS)

install: xthi xthi.nompi
	$(INSTALL) -D xthi $(DESTDIR)$(prefix)/bin/xthi
	$(INSTALL) -D xthi.nompi $(DESTDIR)$(prefix)/bin/xthi.nompi

clean:
	-rm -f xthi xthi.nompi

distclean: clean

uninstall:
	-rm -f $(DESTDIR)$(prefix)/bin/xthi
	-rm -f $(DESTDIR)$(prefix)/bin/xthi.nompi

.PHONY: all install clean distclean uninstall
